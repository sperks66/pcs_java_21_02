package example1;

/**
 * 16.11.2021
 * 16. Inheritance and Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Figure {
    public double getPerimeter() {
        return -1;
    }

    public void move(int newX, int newY) {

    }
}
